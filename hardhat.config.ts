import dotenv from "dotenv";
import { HardhatUserConfig, task } from "hardhat/config";
import "@nomiclabs/hardhat-etherscan";
import "@nomiclabs/hardhat-waffle";
import "@typechain/hardhat";
import "hardhat-gas-reporter";
import "solidity-coverage";
import { mint } from "./scripts/mint";
import { airdrop } from "./scripts/airdrop";

dotenv.config();

const MNEMONIC = process.env.MNEMONIC;
const ALCHEMY_KEY_TESTNET = process.env.ALCHEMY_KEY_TESTNET;
const mumbaiNodeUrl = `https://polygon-mumbai.g.alchemy.com/v2/${ALCHEMY_KEY_TESTNET}`;
const polygonNodeUrl = `https://polygon-rpc.com`;

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

task("mint", "Mint NFT from the contract", mint)
  .addParam("quantity", "Amount of NFTs to mint")
  .addParam("folder", "IPFS folder used for minting");

task("airdrop", "Aidrop NFTs from the contract", airdrop)
  .addParam("file", "Path to file used for the airdrop")
  .addParam("folder", "IPFS folder used for airdropping");

const config: HardhatUserConfig = {
  solidity: {
    version: "0.8.16",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200,
      },
    },
  },
  networks: {
    development: { url: "http://127.0.0.1:8545/" },
    mumbai: { url: mumbaiNodeUrl, accounts: { mnemonic: MNEMONIC } },
    polygon: { url: polygonNodeUrl, accounts: { mnemonic: MNEMONIC } },
  },
  gasReporter: {
    enabled: process.env.REPORT_GAS !== undefined,
    currency: "USD",
  },
  etherscan: {
    apiKey: {
      polygon: process.env.POLYSCAN_KEY ?? "",
      polygonMumbai: process.env.POLYSCAN_KEY ?? "",
    },
  },
};

export default config;
