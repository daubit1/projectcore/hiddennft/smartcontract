import { readFileSync, writeFileSync } from "fs";

export class IO {
  path = "contracts/addresses.json";
  addresses: any;

  constructor() {
    this.addresses = {};
  }

  fetch(network: number) {
    this.addresses = JSON.parse(readFileSync(this.path, "utf8"));
    return this.addresses[network.toString()] || {};
  }

  save(network: number, addresses: any) {
    this.addresses[network] = { ...this.addresses[network], ...addresses };
    const result = JSON.stringify(this.addresses, null, 2);
    writeFileSync(this.path, result);
  }
}
