import { getGCSAttribs } from "./metadata-gcs";

export function getAttribsFromFilename(type: string, filename: string) {
  if (type === "gcs") {
    return getGCSAttribs(filename);
  } else {
    console.log(`invalid type ${type}`);
    throw new Error(`invalid type ${type}`);
  }
}

export function getName(
  category: string,
  // eslint-disable-next-line camelcase
  metadataAttribs: { trait_type: string; value: string }[],
  metadataTemplate: Record<string, unknown>
) {
  if (category === "gcs") {
    return `${metadataTemplate.name}`;
  } else {
    throw new Error(`unknown category ${category}`);
  }
}

export function getDescription(
  category: string,
  // eslint-disable-next-line camelcase
  metadataAttribs: { trait_type: string; value: string }[],
  metadataTemplate: Record<string, unknown>
) {
  if (category === "gcs") {
    const type = metadataAttribs.find((x) => x.trait_type === "Type")!.value;
    let cc = "";
    let sc = "";
    let bg = "";

    switch (
      metadataAttribs.find((x) => x.trait_type === "Background Color")?.value
    ) {
      case "Blue":
        bg = "into the blue";
        break;
      case "Pink":
        bg = "on pink clouds";
        break;
      case "Yellow":
        bg = "to the sun";
        break;
      default:
        throw new Error(`invalid bg`);
    }

    switch (
      metadataAttribs.find((x) => x.trait_type === "Skateboard Color")?.value
    ) {
      case "Green":
        sc = "while riding confused";
        break;
      case "Red":
        sc = "while riding fast";
        break;
      case "Blue":
        sc = "while riding drunk";
        break;
      case "Pink":
        sc = "while riding fancy";
        break;
      default:
        throw new Error(`invalid bg`);
    }

    switch (
      metadataAttribs.find((x) => x.trait_type === "Controller Color")?.value
    ) {
      case "Red":
        cc = "plays aggressively";
        break;
      case "Pink":
        cc = "plays softly";
        break;
      case "Blue":
        cc = "plays chilled";
        break;
      default:
        throw new Error(`invalid bg`);
    }

    if (type === "" || cc === "" || sc === "" || bg === "") {
      throw new Error(`${type} ${cc} ${sc} ${bg}`);
    }
    return `${type} ${cc} ${sc} ${bg}`;
  } else {
    throw new Error(`unknown category ${category}`);
  }
}
