function getBackgroundColor(color: string) {
  let value = "";
  switch (color) {
    case "BGBL":
      value = "Blue";
      break;
    case "BGPI":
      value = "Pink";
      break;
    case "BGYE":
      value = "Yellow";
      break;
    default:
      throw new Error(`invalid backgroundcolor type ${color}`);
  }
  return { trait_type: "Background Color", value: value };
}

function getType(color: string) {
  let value = "";
  switch (color) {
    case "AS":
      value = "Astroboy";
      break;
    case "AL":
      value = "Alien";
      break;
    case "OR":
    case "_OR":
      value = "Orc";
      break;
    default:
      throw new Error(`type ${color}`);
  }
  return { trait_type: "Type", value: value };
}

function getControllerColor(color: string) {
  let value = "";
  switch (color) {
    case "CTRE":
      value = "Red";
      break;
    case "CTPI":
      value = "Pink";
      break;
    case "CTBL":
      value = "Blue";
      break;
    default:
      throw new Error(`invalid controller color type ${color}`);
  }
  return { trait_type: "Controller Color", value: value };
}

function getSkateboardColor(color: string) {
  let value = "";
  switch (color) {
    case "SKGR":
      value = "Green";
      break;
    case "SKRE":
      value = "Red";
      break;
    case "SKBL":
      value = "Blue";
      break;
    case "SKPI":
      value = "Pink";
      break;
    default:
      throw new Error(`invalid skateboard color type ${color}`);
  }
  return { trait_type: "Skateboard Color", value: value };
}

export function getGCSAttribs(filename: string) {
  const filenameWoExt = filename.split(".")[0];
  const splitFilenames = filenameWoExt.split("-");
  if (splitFilenames.length !== 4) {
    throw new Error(
      `filename ${filename} does not match kartal troll scheme, length ${splitFilenames.length}`
    );
  }
  // eslint-disable-next-line camelcase
  const attribs: { trait_type: string; value: string }[] = [];
  attribs.push(getType(splitFilenames[0]));
  attribs.push(getControllerColor(splitFilenames[3]));
  attribs.push(getSkateboardColor(splitFilenames[2]));
  attribs.push(getBackgroundColor(splitFilenames[1]));
  return attribs;
}
