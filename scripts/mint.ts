import { IO } from "../util/io";
import { MFSWrapper } from "../util/ipfs";
import { HardhatRuntimeEnvironment } from "hardhat/types";

const NETWORK_URL: { [chainId: number]: string } = {
  80001: "https://mumbai.polygonscan.com/tx",
  137: "https://polygonscan.com/tx",
};

const networkUrl = (chainId: number) =>
  NETWORK_URL[chainId]
    ? NETWORK_URL[chainId]
    : new Error("Cannot find chain name");

interface TaskArgs {
  address: string;
  quantity: string;
  folder: string;
}

export async function mint(taskArgs: TaskArgs, hre: HardhatRuntimeEnvironment) {
  const quantitiy = parseInt(taskArgs.quantity || "");
  const folder = taskArgs.folder;
  const address = taskArgs.address;
  if (isNaN(quantitiy)) throw new Error("Invalid value");
  if (folder === undefined) throw new Error("Require folder name");
  const io = new IO();
  const mfs = new MFSWrapper();
  const { provider } = hre.ethers;
  const chainId = (await provider.getNetwork()).chainId;
  let amountFilesOnIPFS: number;
  try {
    amountFilesOnIPFS = (await mfs.ls(`/${folder}`)).length;
  } catch (e) {
    throw new Error("Folder does not exists");
  }

  const { hiddenNft: hiddenNftAddress } = io.fetch(chainId);
  if (!hiddenNftAddress) throw new Error("Need an address!");
  const HiddenNFT = await hre.ethers.getContractFactory("HiddenNFT");
  const hiddenNFT = HiddenNFT.attach(hiddenNftAddress);

  const totalSupply = (await hiddenNFT.totalSupply()).toNumber();
  if (amountFilesOnIPFS < totalSupply + quantitiy)
    throw new Error("IPFS does not contain enough files for minting!");
  const mint = await hiddenNFT.mint(address, quantitiy);
  console.log(`Attempting to mint ${quantitiy} NFTs...`);
  await mint.wait();
  console.log(`${networkUrl(chainId)}/${mint.hash}`);
}
