import {
  getAttribsFromFilename,
  getName,
  getDescription,
} from "../util/metadata";
import { readFile, writeFile, readdir } from "fs/promises";

let lookup = {};

const NFT_TYPE = "gcs";

const removeFromList = [
  "OR-BGPI-SKPI-CTRE.png",
  "OR-BGPI-SKRE-CTPI.png",
  "OR-BGYE-SKPI-CTRE.png",
  "OR-BGYE-SKRE-CTPI.png",
  "OR-BGYE-SKGR-CTBL.png",
  "OR-BGPI-SKGR-CTBL.png",
  "OR-BGBL-SKPI-CTRE.png",
  "OR-BGBL-SKRE-CTPI.png",
  "OR-BGBL-SKGR-CTPI.png",
];

const holdbackFromList = [
  "AS-BGBL-SKBL-CTBL.png",
  "AS-BGBL-SKPI-CTPI.png",
  "AS-BGPI-SKPI-CTPI.png",
  "AS-BGPI-SKBL-CTBL.png",
  "AS-BGYE-SKBL-CTBL.png",
  "_OR-BGBL-SKBL-CTBL.png",
  "AL-BGBL-SKRE-CTRE.png",
  "AL-BGPI-SKRE-CTRE.png",
  "AL-BGBL-SKBL-CTBL.png",
];

function shuffle<T>(array: T[]) {
  array.sort(() => Math.random() - 0.5);
}

async function preuploadMetadata() {
  const metadataTemplate = JSON.parse(
    await readFile("./data/metadatatemplate.json", { encoding: "utf-8" })
  );
  const cids = JSON.parse(
    await readFile("./data/cids/image-ipfs-lookup-gcs.json", {
      encoding: "utf-8",
    })
  );
  const artFiles = (await readdir("./data/art")).filter(
    (a) => !a.includes(".gitkeep")
  );
  const fakeLookup: Record<string, string> = {};
  artFiles.forEach((a) => (fakeLookup[a] = a));

  let lookuptableEntries = Object.entries(fakeLookup);
  const all = lookuptableEntries.filter((x) => !removeFromList.includes(x[0]));
  const holdback = all.filter((x) => holdbackFromList.includes(x[0]));
  const allWoHoldback = all.filter((x) => !holdbackFromList.includes(x[0]));
  shuffle(all);
  lookuptableEntries = [...holdback, ...allWoHoldback];

  const metadataArray: { data: Object; filename: string }[] = [];
  for (const [i, artlookup] of lookuptableEntries.entries()) {
    const metadataAttribs = getAttribsFromFilename(NFT_TYPE, artlookup[0]);
    const metadata = {
      ...metadataTemplate,
      image: `ipfs://${cids[artlookup[1]]}`,
      attributes: metadataAttribs,
      name: `${getName(NFT_TYPE, metadataAttribs, metadataTemplate)} #${i + 1}`,
      description: getDescription(NFT_TYPE, metadataAttribs, metadataTemplate),
    };
    metadataArray.push({ data: metadata, filename: artlookup[0] });
  }

  lookup = metadataArray;
}

preuploadMetadata()
  .then(() => {
    writeFile(`./data/metadata-dryrun-gcs.json`, JSON.stringify(lookup));
  })
  .catch((err) => {
    console.log(err);
    writeFile(`./data/metadata-dryrun-gcs.json`, JSON.stringify(lookup));
    // eslint-disable-next-line no-process-exit
    process.exit(1);
  });
