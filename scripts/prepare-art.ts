const fs = require("fs").promises;

async function main() {
  const folders = await fs.readdir("./data/art-raw");
  console.log(folders);
  for (const folder of folders) {
    const files = await fs.readdir(`./data/art-raw/${folder}`);
    for (const file of files) {
      fs.rename(
        `./data/art-raw/${folder}/${file}`,
        `./data/art-raw/${folder}/${folder}-${file}`
      );
    }
  }

  for (const folder of folders) {
    const files = await fs.readdir(`./data/art-raw/${folder}`);
    for (const file of files) {
      fs.rename(`./data/art-raw/${folder}/${file}`, `./data/art/${file}`);
    }
  }
}

// eslint-disable-next-line no-process-exit
main().then(() => process.exit(0));
