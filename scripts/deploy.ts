import { IO } from "../util/io";
import hardhat, { ethers } from "hardhat";
import { CONTRACT_CID, FOLDER_CID, REGISTRY_ADDRESS } from "../util/const.json";

const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

const NETWORK_NAME: { [chainId: number]: string } = {
  80001: "mumbai",
  137: "polygon",
  1337: "development",
};

const networkName = (chainId: number) =>
  NETWORK_NAME[chainId]
    ? NETWORK_NAME[chainId]
    : new Error("Cannot find chain name");

async function main() {
  const io = new IO();
  const { provider } = ethers;
  const chainId = (await provider.getNetwork()).chainId;
  const { hiddenNft: hiddenNftAddress } = io.fetch(chainId);
  const HiddenNFT = await ethers.getContractFactory("HiddenNFT");
  const hiddenNFT = await HiddenNFT.deploy(
    FOLDER_CID,
    CONTRACT_CID,
    REGISTRY_ADDRESS
  );
  if (!hiddenNftAddress) {
    await hiddenNFT.deployed();
    io.save(chainId, { hiddenNFT: hiddenNFT.address });
    console.log("hiddenNFT deployed to:", hiddenNFT.address);
    console.log("Waiting for verification...");
    await sleep(60 * 1000);
    hardhat.run("verify", {
      address: hiddenNFT.address,
      network: networkName(chainId),
      constructorArgsParams: [FOLDER_CID, CONTRACT_CID, REGISTRY_ADDRESS],
    });
    const mintRole = await hiddenNFT.MINTER_ROLE();

    for (const addr of [
      "0x645c2c344Da013BD3F9AE228eC9b3ED4Bd65662c",
      "0x8133b92cBC2be6341a6F4Eb76b7D8319699eBe73",
      "0x31D678EF4652408Dda0e395Eb271dfBE50835a7d",
    ]) {
      const tx = await hiddenNFT.grantRole(mintRole, addr);
      await tx.wait();
    }
  } else {
    console.log("Already deployed");
  }
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
