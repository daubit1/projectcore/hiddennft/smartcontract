import { expect } from "chai";
import { ethers } from "hardhat";
import { Contract } from "ethers";
import {
  REGISTRY_ADDRESS,
  CONTRACT_CID,
  MINTER_ROLE,
  FOLDER_CID,
} from "../util/const.json";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

describe("HiddenNFT", async () => {
  const setup = async () => {
    const signers = await ethers.getSigners();
    user = signers.shift();
    admin = signers.shift();
    minter = signers.shift();
    const HiddenNFT = await ethers.getContractFactory("HiddenNFT");
    hiddenNFT = await HiddenNFT.connect(admin!).deploy(
      FOLDER_CID,
      CONTRACT_CID,
      REGISTRY_ADDRESS
    );
    await hiddenNFT.deployed();
  };
  // Setting up accounts
  let hiddenNFT: Contract;
  let user: SignerWithAddress | undefined;
  let admin: SignerWithAddress | undefined;
  let minter: SignerWithAddress | undefined;

  before(setup);
  describe("Contract Information", () => {
    it("name()", async () => {
      const name = await hiddenNFT.name();
      expect(name).to.equal("HiddenNFT");
    });
    it("symbol()", async () => {
      const symbol = await hiddenNFT.symbol();
      expect(symbol).to.equal("HSP");
    });
    it("contractURI()", async () => {
      const contractCID = await hiddenNFT.contractCID();
      expect(contractCID).to.equal(`ipfs://${CONTRACT_CID}`);
    });
  });
  describe("Admin", () => {
    it("can set new contract CID", async () => {
      const newContractCID = "v2";
      await hiddenNFT.setContractCID(newContractCID);
      const result = await hiddenNFT.contractCID();
      expect(result).to.be.equal(`ipfs://${newContractCID}`);
    });
    it("can set new folder CID", async () => {
      const newFolderCID = "v2";
      expect(hiddenNFT.setContractCID(newFolderCID)).to.not.be.reverted;
    });
  });
  describe("Mint", () => {
    beforeEach(setup);
    it("Admin can mint", async () => {
      const quantity = 1;
      const mint = await hiddenNFT.mint(admin!.address, quantity);
      await mint.wait();
      const balance = await hiddenNFT.balanceOf(admin!.address);
      expect(balance).to.be.equal(quantity);
      const tokenURI = await hiddenNFT.tokenURI(0);
      expect(tokenURI).to.be.equal(`ipfs://${FOLDER_CID}/0.json`);
    });
    it("Minter can mint", async () => {
      await hiddenNFT.grantRole(MINTER_ROLE, minter!.address);
      const quantity = 1;
      const tokenId = 0;
      const mint = await hiddenNFT
        .connect(minter!)
        .mint(minter!.address, quantity);
      await mint.wait();
      const balance = await hiddenNFT.balanceOf(minter!.address);
      expect(balance).to.be.equal(quantity);
      const tokenURI = await hiddenNFT.tokenURI(tokenId);
      expect(tokenURI).to.be.equal(`ipfs://${FOLDER_CID}/${tokenId}.json`);
    });
    it("User can NOT mint", async () => {
      expect(hiddenNFT.connect(user!).mint(user!.address, 1)).to.be.reverted;
    });
  });
  describe("Burn", () => {
    beforeEach(setup);
    it("Token owner can burn", async () => {
      const quantity = 1;
      const mint = await hiddenNFT.mint(admin!.address, quantity);
      await mint.wait();
      let balance = await hiddenNFT.balanceOf(admin!.address);
      expect(balance).to.be.equal(quantity);
      const burn = await hiddenNFT.burn(0);
      await burn.wait();
      balance = await hiddenNFT.balanceOf(admin!.address);
      expect(balance).to.be.equal(0);
    });
    it("Non Token owner can NOT burn", async () => {
      const quantity = 1;
      const mint = await hiddenNFT.mint(admin!.address, quantity);
      await mint.wait();
      const balance = await hiddenNFT.balanceOf(admin!.address);
      expect(balance).to.be.equal(quantity);
      const burn = hiddenNFT.connect(minter!).burn(0);
      expect(burn).to.be.reverted;
    });
  });
});
