//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.16;

import "erc721a/contracts/ERC721A.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "./common/meta-transactions/ContentMixin.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "./common/meta-transactions/NativeMetaTransaction.sol";

contract OwnableDelegateProxy {}

/**
 * Used to delegate ownership of a contract to another address, to save on unneeded transactions to approve contract use for users
 */
contract ProxyRegistry {
    mapping(address => OwnableDelegateProxy) public proxies;
}

contract HiddenNFT is
    ERC721A,
    AccessControl,
    ContextMixin,
    NativeMetaTransaction
{
    using Strings for uint256;
    string private folderCID;
    string private contractCID_;

    address private proxyRegistryAddress;

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    constructor(
        string memory _folderCID,
        string memory _contractCID,
        address _proxyRegistryAddress
    ) ERC721A("GC Souvenir", "GCS") {
        folderCID = _folderCID;
        contractCID_ = _contractCID;
        proxyRegistryAddress = _proxyRegistryAddress;

        _grantRole(MINTER_ROLE, msg.sender);
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }

    function _startTokenId() internal view override returns (uint256) {
        return 1;
    }

    function _baseURI() internal pure override returns (string memory) {
        return "ipfs://";
    }

    function setFolderCID(string memory folderCID_)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        folderCID = folderCID_;
    }

    function setContractCID(string memory _contractCID)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        contractCID_ = _contractCID;
    }

    /**
     * @dev Returns the contract CID.
     */
    function contractCID() external view returns (string memory) {
        return string(abi.encodePacked(_baseURI(), contractCID_));
    }

    function tokenURI(uint256 tokenId)
        public
        view
        virtual
        override
        returns (string memory)
    {
        if (!_exists(tokenId)) revert URIQueryForNonexistentToken();
        return string(abi.encodePacked(_baseURI(), folderCID, "/", tokenId.toString()));
    }

    function mint(address to, uint256 quantity) external onlyRole(MINTER_ROLE) {
        require(balanceOf(to) == 0, "Already minted");
        require(totalSupply() + quantity < 100, "only 99 nfts can exits");
        _safeMint(to, quantity);
    }

    /**
     * @dev Override isApprovedForAll to whitelist user's OpenSea proxy accounts to enable gas-less listings.
     */
    function isApprovedForAll(address owner, address operator)
        public
        view
        override
        returns (bool)
    {
        // Whitelist OpenSea proxy contract for easy trading.
        ProxyRegistry proxyRegistry = ProxyRegistry(proxyRegistryAddress);
        if (address(proxyRegistry.proxies(owner)) == operator) {
            return true;
        }

        return super.isApprovedForAll(owner, operator);
    }

    /**
     * @dev This is used instead of msg.sender as transactions won't be sent by the original token owner, but by OpenSea.
     */
    function _msgSender() internal view override returns (address sender) {
        return ContextMixin.msgSender();
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721A, AccessControl)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
}
